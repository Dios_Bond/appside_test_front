import React from 'react';

export const Navbar: React.FC = () => (
    <nav>
        <ul className='navbar'>
            <li><a href='/'>Home</a></li>
        </ul>

    </nav>
)