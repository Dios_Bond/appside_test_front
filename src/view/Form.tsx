import React, {useState} from 'react';
import {config} from '../config/default';
import AsyncSelect from 'react-select/async';
import {LineChart, Line, XAxis, YAxis, CartesianGrid, Tooltip, Legend} from 'recharts';

const urlReq: string = config.serverconnect.link + ":" + config.serverconnect.port;


function createList(data) {
    let dataRes = data.dataRes
    
    let unicCountry = Array.from( new Set (dataRes.map(el => {
        return el.country
    })));

    function getParamByCountry (namedParam) {
        let statist = {
            max: 0,
            min: 0
        };
        let rasArr = unicCountry.map(nameCountry => {
            let arr = dataRes
                .filter(el => (nameCountry == el.country)).map(ell => {return ell[namedParam]});

            statist.max = Math.max(...arr);
            statist.min = Math.min(...arr);
            

        })
        return statist.max - statist.min
    }
    
    
    return (unicCountry.map(el => {
       return (
       <li>In country {el} confirmed {
        getParamByCountry("confirmed")
        } deaths {getParamByCountry("deaths")
        } recovered {getParamByCountry("recovered")}
         </li>)
    })
      
    )
  };


const Forma: React.FC = ()=> {
    let initState: IData = {         
            country: 'Afghanistan',
            dateStart: '',
            dateEnd: '',
            dataRes: [],
    };
    interface IData {
        country: string
        dateStart: string
        dateEnd: string
        dataRes: Array<IRes>
    };

    interface IRes {
        country: string
        date: string
        confirmed: number
        deaths: number
        recovered: number
    };

    // interface IOptions { 
    //     [index: number]: 
    //     {
    //         value: string;
    //         label: string

    //     }
    //     | undefined
    //     // ({ value: string
    //     // label: string } | { value: string
    //     //      label: string; } | undefined)
    // }
    

    const [state, setState] = useState<Object>(initState);
    

    const changeState = (event: React.ChangeEvent<HTMLInputElement>) => {
        setState({...state, [event.target.name]: event.target.value});
    }

    async function changeStateData(prom: Promise<any>) {
        setState({...state, dataRes: await prom});
    }

    function stringData (str: string): string {
        let dateMs: Date = new Date(Date.parse(str));
        let strDate: string = `${dateMs.getFullYear()}-${dateMs.getMonth()+1}-${dateMs.getDate()}`;
        return strDate
    };

    function getCountryList () {
        let dataProm: any = new Promise ((res, rej) => {
            fetch (`${urlReq}/getallcountry` ,{
                method: "GET"
            })
            .then( res => res.json())
            .then( data => {
                    let resArr = data.map((el: string) => {
                        return {value: el, label: el}
                    });
                    res(resArr)
            });
        })
        return dataProm
    };


    const formSubmit = async (event: React.ChangeEvent<HTMLFormElement>) => {
        event.preventDefault();
        let newdata: any = state;
        let dataProm = new Promise ((res, rej) => {
            fetch (`${urlReq}/getdata/?country=${newdata.country}&dateStart=${stringData(newdata.dateStart)}&dateEnd=${stringData(newdata.dateEnd)}` ,{
                method: "GET"
            })
            .then( res => res.json())
            .then( data => {
                    console.log (data);
                    res(data)
            });
        })
        changeStateData(dataProm)
    };

    function RetData (dat: any) {
        return dat.dataRes
    };

    function RetDataCountry (dat: any) {
        return dat.country
    };

    function changeSelect (event: any) {
        let arrEvent = event.map((el: any) => {
            return `"${el.value}"`
        });
        setState({...state, country: arrEvent});
    };

    return (
  <div>
      <script src="https://unpkg.com/react-dom/umd/react-dom.production.min.js"></script>

        <form className = 'formView' onSubmit = {formSubmit}>
            <label>
                Input Country:
                {/* <input list = 'country' name = 'country' id = 'country' type = 'text' onChange = {changeState} value = {RetDataCountry(state)}></input>  */}
                {/* <select name = 'country' id = 'country' value = {RetDataCountry(state)}>
                {/* {createOptions()} */}
                {/* </select>  */}
                <AsyncSelect 
                // defaultValue = {[{value: 'Afghanistan', label: 'Afghanistan'}]}
                isMulti
                name = 'country'
                cacheOptions
                defaultOptions
                loadOptions = {getCountryList}
                // loadOptions = {getCountryList}
                // options = {createOptions()}
                onChange = {changeSelect}
                />
                
            </label>
            <br />
            <label>
                From:
                <input name = 'dateStart' type = 'date' onChange = {changeState} data-date-format="YYYY-M-D"></input>
            </label>
            <br />
            <label>
                To:
                <input name = 'dateEnd' type = 'date' onChange = {changeState}></input>
            </label>
            <br />
            <input type = 'submit' value = 'Search'></input>
        </form>

        {/* <h1>{RetDataCountry(state)}</h1> */}
        <ul>
            {createList(state)}
        </ul>
        {/* <LineChart
                
            width={800}
            height={500}
            data = {RetData(state)}
            margin={{
            top: 5, right: 30, left: 20, bottom: 5,
            }}
            >
            <CartesianGrid strokeDasharray="3 3" />
            <XAxis dataKey="date" />
            <YAxis dataKey="confirmed"/>
            <Tooltip />
            <Legend />
            <Line type="monotone" dataKey="confirmed" stroke="#8884d8" activeDot={{ r: 8 }} />
            <Line type="monotone" dataKey="deaths" stroke="#ff0000" activeDot={{ r: 8 }} />
            <Line type="monotone" dataKey="recovered" stroke="#00ff00" activeDot={{ r: 8 }} />
            <Line type="monotone" dataKey="date" stroke="#82ca9d" />
        </LineChart> */}


        </div>
    );
  }
  
  export default Forma;